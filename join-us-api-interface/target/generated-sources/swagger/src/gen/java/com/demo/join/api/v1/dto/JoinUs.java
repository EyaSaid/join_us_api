package com.demo.join.api.v1.dto;

import javax.validation.constraints.*;
import javax.validation.Valid;


import io.swagger.annotations.*;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;


public class JoinUs   {
  
  private @Valid String nom = null;
  private @Valid String prenom = null;
  private @Valid String email = null;
  private @Valid String dateDeNaissance = null;
  private @Valid String delegation = null;
  private @Valid String gouvernaurat = null;
  private @Valid String address = null;
  private @Valid String niveauEducatif = null;
  private @Valid String vousEtes = null;
  private @Valid String etablissement = null;
  private @Valid String specialite = null;
  private @Valid String emploi = null;
  private @Valid String numTelephone = null;
  private @Valid String scout = null;

  /**
   **/
  public JoinUs nom(String nom) {
    this.nom = nom;
    return this;
  }

  
  @ApiModelProperty(value = "")
  @JsonProperty("nom")
  public String getNom() {
    return nom;
  }
  public void setNom(String nom) {
    this.nom = nom;
  }

  /**
   **/
  public JoinUs prenom(String prenom) {
    this.prenom = prenom;
    return this;
  }

  
  @ApiModelProperty(value = "")
  @JsonProperty("prenom")
  public String getPrenom() {
    return prenom;
  }
  public void setPrenom(String prenom) {
    this.prenom = prenom;
  }

  /**
   **/
  public JoinUs email(String email) {
    this.email = email;
    return this;
  }

  
  @ApiModelProperty(value = "")
  @JsonProperty("email")
  public String getEmail() {
    return email;
  }
  public void setEmail(String email) {
    this.email = email;
  }

  /**
   **/
  public JoinUs dateDeNaissance(String dateDeNaissance) {
    this.dateDeNaissance = dateDeNaissance;
    return this;
  }

  
  @ApiModelProperty(value = "")
  @JsonProperty("dateDeNaissance")
  public String getDateDeNaissance() {
    return dateDeNaissance;
  }
  public void setDateDeNaissance(String dateDeNaissance) {
    this.dateDeNaissance = dateDeNaissance;
  }

  /**
   **/
  public JoinUs delegation(String delegation) {
    this.delegation = delegation;
    return this;
  }

  
  @ApiModelProperty(value = "")
  @JsonProperty("delegation")
  public String getDelegation() {
    return delegation;
  }
  public void setDelegation(String delegation) {
    this.delegation = delegation;
  }

  /**
   **/
  public JoinUs gouvernaurat(String gouvernaurat) {
    this.gouvernaurat = gouvernaurat;
    return this;
  }

  
  @ApiModelProperty(value = "")
  @JsonProperty("gouvernaurat")
  public String getGouvernaurat() {
    return gouvernaurat;
  }
  public void setGouvernaurat(String gouvernaurat) {
    this.gouvernaurat = gouvernaurat;
  }

  /**
   **/
  public JoinUs address(String address) {
    this.address = address;
    return this;
  }

  
  @ApiModelProperty(value = "")
  @JsonProperty("address")
  public String getAddress() {
    return address;
  }
  public void setAddress(String address) {
    this.address = address;
  }

  /**
   **/
  public JoinUs niveauEducatif(String niveauEducatif) {
    this.niveauEducatif = niveauEducatif;
    return this;
  }

  
  @ApiModelProperty(value = "")
  @JsonProperty("niveauEducatif")
  public String getNiveauEducatif() {
    return niveauEducatif;
  }
  public void setNiveauEducatif(String niveauEducatif) {
    this.niveauEducatif = niveauEducatif;
  }

  /**
   **/
  public JoinUs vousEtes(String vousEtes) {
    this.vousEtes = vousEtes;
    return this;
  }

  
  @ApiModelProperty(value = "")
  @JsonProperty("vousEtes")
  public String getVousEtes() {
    return vousEtes;
  }
  public void setVousEtes(String vousEtes) {
    this.vousEtes = vousEtes;
  }

  /**
   **/
  public JoinUs etablissement(String etablissement) {
    this.etablissement = etablissement;
    return this;
  }

  
  @ApiModelProperty(value = "")
  @JsonProperty("etablissement")
  public String getEtablissement() {
    return etablissement;
  }
  public void setEtablissement(String etablissement) {
    this.etablissement = etablissement;
  }

  /**
   **/
  public JoinUs specialite(String specialite) {
    this.specialite = specialite;
    return this;
  }

  
  @ApiModelProperty(value = "")
  @JsonProperty("specialite")
  public String getSpecialite() {
    return specialite;
  }
  public void setSpecialite(String specialite) {
    this.specialite = specialite;
  }

  /**
   **/
  public JoinUs emploi(String emploi) {
    this.emploi = emploi;
    return this;
  }

  
  @ApiModelProperty(value = "")
  @JsonProperty("emploi")
  public String getEmploi() {
    return emploi;
  }
  public void setEmploi(String emploi) {
    this.emploi = emploi;
  }

  /**
   **/
  public JoinUs numTelephone(String numTelephone) {
    this.numTelephone = numTelephone;
    return this;
  }

  
  @ApiModelProperty(value = "")
  @JsonProperty("numTelephone")
  public String getNumTelephone() {
    return numTelephone;
  }
  public void setNumTelephone(String numTelephone) {
    this.numTelephone = numTelephone;
  }

  /**
   **/
  public JoinUs scout(String scout) {
    this.scout = scout;
    return this;
  }

  
  @ApiModelProperty(value = "")
  @JsonProperty("scout")
  public String getScout() {
    return scout;
  }
  public void setScout(String scout) {
    this.scout = scout;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    JoinUs joinUs = (JoinUs) o;
    return Objects.equals(nom, joinUs.nom) &&
        Objects.equals(prenom, joinUs.prenom) &&
        Objects.equals(email, joinUs.email) &&
        Objects.equals(dateDeNaissance, joinUs.dateDeNaissance) &&
        Objects.equals(delegation, joinUs.delegation) &&
        Objects.equals(gouvernaurat, joinUs.gouvernaurat) &&
        Objects.equals(address, joinUs.address) &&
        Objects.equals(niveauEducatif, joinUs.niveauEducatif) &&
        Objects.equals(vousEtes, joinUs.vousEtes) &&
        Objects.equals(etablissement, joinUs.etablissement) &&
        Objects.equals(specialite, joinUs.specialite) &&
        Objects.equals(emploi, joinUs.emploi) &&
        Objects.equals(numTelephone, joinUs.numTelephone) &&
        Objects.equals(scout, joinUs.scout);
  }

  @Override
  public int hashCode() {
    return Objects.hash(nom, prenom, email, dateDeNaissance, delegation, gouvernaurat, address, niveauEducatif, vousEtes, etablissement, specialite, emploi, numTelephone, scout);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class JoinUs {\n");
    
    sb.append("    nom: ").append(toIndentedString(nom)).append("\n");
    sb.append("    prenom: ").append(toIndentedString(prenom)).append("\n");
    sb.append("    email: ").append(toIndentedString(email)).append("\n");
    sb.append("    dateDeNaissance: ").append(toIndentedString(dateDeNaissance)).append("\n");
    sb.append("    delegation: ").append(toIndentedString(delegation)).append("\n");
    sb.append("    gouvernaurat: ").append(toIndentedString(gouvernaurat)).append("\n");
    sb.append("    address: ").append(toIndentedString(address)).append("\n");
    sb.append("    niveauEducatif: ").append(toIndentedString(niveauEducatif)).append("\n");
    sb.append("    vousEtes: ").append(toIndentedString(vousEtes)).append("\n");
    sb.append("    etablissement: ").append(toIndentedString(etablissement)).append("\n");
    sb.append("    specialite: ").append(toIndentedString(specialite)).append("\n");
    sb.append("    emploi: ").append(toIndentedString(emploi)).append("\n");
    sb.append("    numTelephone: ").append(toIndentedString(numTelephone)).append("\n");
    sb.append("    scout: ").append(toIndentedString(scout)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

