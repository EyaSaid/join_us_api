package com.demo.join.api.v1;

import com.demo.join.api.v1.dto.ApplicationError;
import com.demo.join.api.v1.dto.JoinUs;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

import io.swagger.annotations.*;

import java.util.Map;
import java.util.List;
import javax.validation.constraints.*;
import javax.validation.Valid;

@Path("/joinUs")
@Api(description = "the joinUs API")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaJAXRSSpecServerCodegen", date = "2021-05-30T13:05:34.527+01:00")
public interface JoinUsApi {

    @DELETE
    @Path("/{memberId}")
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    @ApiOperation(value = "delete member", notes = "", tags={ "joinUs",  })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Success", response = JoinUs.class, responseContainer = "List"),
        @ApiResponse(code = 500, message = "internal server error", response = ApplicationError.class, responseContainer = "List") })
    Response deleteMember(@PathParam("memberId") @ApiParam("Member unique identifier") String memberId);

    @POST
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    @ApiOperation(value = "send member", notes = "", tags={ "joinUs" })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Success", response = JoinUs.class, responseContainer = "List"),
        @ApiResponse(code = 500, message = "internal server error", response = ApplicationError.class, responseContainer = "List") })
    Response sendMember(@Valid JoinUs member);
}
