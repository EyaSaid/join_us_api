package com.tunisian.scout.joinUs.api.model;


import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table( name = "joinus",schema = "joinusdb")
public class JoinUsEntity {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String nom;
    private String prenom;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDateDeNaissance() {
        return dateDeNaissance;
    }

    public void setDateDeNaissance(String dateDeNaissance) {
        this.dateDeNaissance = dateDeNaissance;
    }

    public String getDelegation() {
        return delegation;
    }

    public void setDelegation(String delegation) {
        this.delegation = delegation;
    }

    public String getGouvernaurat() {
        return gouvernaurat;
    }

    public void setGouvernaurat(String gouvernaurat) {
        this.gouvernaurat = gouvernaurat;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNiveauEducatif() {
        return niveauEducatif;
    }

    public void setNiveauEducatif(String niveauEducatif) {
        this.niveauEducatif = niveauEducatif;
    }

    public String getVousEtes() {
        return vousEtes;
    }

    public void setVousEtes(String vousEtes) {
        this.vousEtes = vousEtes;
    }

    public String getEtablissement() {
        return etablissement;
    }

    public void setEtablissement(String etablissement) {
        this.etablissement = etablissement;
    }

    public String getSpecialite() {
        return specialite;
    }

    public void setSpecialite(String specialite) {
        this.specialite = specialite;
    }

    public String getEmploi() {
        return emploi;
    }

    public void setEmploi(String emploi) {
        this.emploi = emploi;
    }

    public String getNumTelephone() {
        return numTelephone;
    }

    public void setNumTelephone(String numTelephone) {
        this.numTelephone = numTelephone;
    }

    public String getScout() {
        return scout;
    }

    public void setScout(String scout) {
        this.scout = scout;
    }

    private String email;
    private String dateDeNaissance;
    private String delegation;
    private String gouvernaurat;
    private String address;
    private String niveauEducatif;
    private String vousEtes;
    private String etablissement;
    private String specialite;
    private String emploi;
    private String numTelephone;
    private String scout;

    public JoinUsEntity(String nom, String prenom, String email, String dateDeNaissance, String delegation, String gouvernaurat, String address, String niveauEducatif, String vousEtes, String etablissement, String specialite, String emploi, String numTelephone, String scout) {
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.dateDeNaissance = dateDeNaissance;
        this.delegation = delegation;
        this.gouvernaurat = gouvernaurat;
        this.address = address;
        this.niveauEducatif = niveauEducatif;
        this.vousEtes = vousEtes;
        this.etablissement = etablissement;
        this.specialite = specialite;
        this.emploi = emploi;
        this.numTelephone = numTelephone;
        this.scout = scout;
    }

    public JoinUsEntity() {

    }
}
